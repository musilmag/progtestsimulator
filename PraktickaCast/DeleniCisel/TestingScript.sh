#!/bin/bash

if [ ! -e ./main.c ]
then
	echo "Dejte do tohoto adresáře (ProgtestSimulator/PraktickaCast/DeleniCisel) svůj zdroják ve tvaru main.c. Pls"
	exit 1
fi 

g++ -Wall -pedantic main.c -o tested.out

red=$'\e[1;31m'
grn=$'\e[1;32m'
cyn=$'\e[1;36m';
end=$'\e[0m';
directories=( "Zakladni" "KontrolaVstupu" "Mezni" );

for dir in {0..2}
do
	printf "${cyn}Testing data: %s\n${end}" ${directories[$dir]};
	chyb=0;
	celkem=0;
	for i in {0000..1000} 
	do
		if [ ! -e "TestingData/${directories[$dir]}/${i}_in.txt" ]; then
			printf "${grn}For directory \"%s\" %d errors out of %d.\n${end}" ${directories[$dir]} $chyb $celkem;
			break;
		fi
		celkem=$(($celkem + 1))
		./tested.out < TestingData/${directories[$dir]}/${i}_in.txt | sort > output.txt;
		cat TestingData/${directories[$dir]}/${i}_out.txt | sort > tmp.txt;
		if cmp -s tmp.txt output.txt; then
			printf "Input %s: okay\n" $i;
		else
			printf "${red}Input %s: ERROR\n${end}" $i;
			chyb=$(($chyb + 1))
		fi
	done
done

printf "${cyn}Testing data: Nahodna\n${end}";
chyb=0;
celkem=0;
for i in {0000..0020}
        do
                celkem=$(($celkem + 1))
		TestingData/Nahodna/generator.out 50 > TestingData/Nahodna/"$i"_in.txt
                TestingData/Nahodna/vzorove.out < TestingData/Nahodna/"$i"_in.txt > TestingData/Nahodna/"$i"_out.txt
		./tested.out < TestingData/Nahodna/${i}_in.txt | sort > output.txt;
                cat TestingData/Nahodna/${i}_out.txt | sort > tmp.txt;
                if cmp -s tmp.txt output.txt; then
                        printf "Input %s: okay\n" $i;
                else
                        printf "${red}Input %s: ERROR\n${end}" $i;
                        chyb=$(($chyb + 1))
                fi
        done

printf "${grn}For directory \"Nahodna\" %d errors out of %d.\n${end}"
printf "${grn}Done.${end}\n"
rm tmp.txt output.txt tested.out
exit 0;
