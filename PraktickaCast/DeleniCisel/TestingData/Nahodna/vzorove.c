#include <stdio.h>


int separate_rec(const char string[101], int i_s, char buffer[200], int i_b){
    //note: i_b points to newly empty place
    int leftToThree = 0;
    int sum = 0;
    while (1){
        buffer[i_b] = string[i_s];
        leftToThree = (leftToThree + string[i_s] - '0')%3;
        i_s++;
        i_b++;
        if (string[i_s] == '\0'){ //this is the end
            if (leftToThree == 0) { //is possible
                buffer[i_b] = '\0';
                printf("%s\n", buffer);
                return sum + 1;
            }
            return sum; //isn't possible
        }
        if (leftToThree == 0){  //can move to another world
            buffer[i_b] = ' ';
            sum += separate_rec(string,i_s,buffer,i_b+1);
        }
    }
}

short readString(char string[101]){
    int i = 0;
    for (; i < 100; ++i) {
        string[i] = fgetc(stdin);
        if (string[i] < '0' || string[i] > '9') break;
        //string[i] -= '0';
    }
    if (i == 0) return -1;
    if (string[i] != '\0' && string[i] != '\n' && string[i] != EOF) return -1;
    string[i] = '\0';
    return 0;
}

int main() {
    char string[101];
    char buffer[200];
    printf("Vstupni retezec:\n");
    if (readString(string)){
        printf("Nespravny vstup.\n");
        return 1;
    }
    int sum = separate_rec(string,0,buffer,0);
    printf("Celkem: %d\n", sum);
    return 0;
}
