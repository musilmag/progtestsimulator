#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int Random(int min, int max){
    return rand()%(max-min+1)+min;
}

int charToInt(char * str){
    int num = 0;
    while (*str != '\0'){
        num *= 10;
        num += *str - '0';
        str++;
    }
    return num;
}

int main(int argc, char *argv[]) {
    srand(time(NULL));
    int howMany = Random(5, charToInt(argv[1]));
    for (int i = 0; i < howMany; ++i) {
        printf("%d", Random(0,9));
    }
    printf("\n");
    return 0;
}
