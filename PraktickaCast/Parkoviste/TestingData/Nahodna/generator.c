#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

char rz_arr [][11] = {"Scout", "Vindicator","Wish", "Paradox","Mystery", "Whim","Falcon", "Epitome","Ranger", "Motion",
                      "Incendie","Portrait","Quête","Rencontre","Souhait","Lumière","Début","Vague","Buffle","Chance",
                      "5AM0514","1A10000","EXELSI0R","1S86599","1S86598","4E71525","3AH9783","2SM1590","4AX0983","4AX0969"};

int Random(int min, int max){
    return rand()%(max-min+1) + min;
}

typedef struct{
    char d [30][11];
    int len;
}Array;

void transfer(Array * to, Array * from, int i){
    strcpy(to->d[to->len], from->d[i]);
    to->len++;
    from->len--;
    strcpy(from->d[i],from->d[from->len]);
}

int main() {
    srand(time(NULL));
    Array parked, empty;
    empty.len = 0;
    for (int i = 0; i < sizeof (rz_arr)/sizeof (rz_arr[0]); ++i) {
        strcpy(empty.d[i],rz_arr[i]);
        empty.len++;
    }
    int howMany = Random(100,500);
    int index;
    for (int i = 0; i < howMany; ++i) {
        int whatToDo = Random(0,3);
        switch (whatToDo) {
            case 2: //removeCar
                if (parked.len == 0) {//park car instead
                    index = Random(0,empty.len-1);
                    printf("+ %s\n", empty.d[index]);
                    transfer(&parked,&empty,index);
                } else {
                    index = Random(0,parked.len-1);
                    printf("- %s\n", parked.d[index]);
                    transfer(&empty,&parked,index);
                }
                break;
            case 3: //remove not parked
            if (empty.len == 0)
                printf("- AA\n");
            else {
                index = Random(0, empty.len - 1);
                printf("- %s\n", empty.d[index]);
            }
                break;
            default:    //parkCar
                if (empty.len == 0) {//remove car instead
                    index = Random(0,parked.len-1);
                    printf("- %s\n", parked.d[index]);
                    transfer(&empty,&parked,index);
                } else{
                    index = Random(0,empty.len-1);
                    printf("+ %s\n", empty.d[index]);
                    transfer(&parked,&empty,index);
                }
                break;
        }
    }
    /*for (int i = 0; i < howMany; ++i) {
        int index = Random(0,sizeof (rz_arr)/sizeof (rz_arr[0])-1);
        int isParking = Random(0,1);
        printf("%c %s\n", isParking ? '+':'-', rz_arr[index]);
    }*/
    return 0;
}
