#include <stdio.h>
#include <string.h>
#include <malloc.h>

typedef struct TEntry{
    char floor;
    int space;
    char rz[11];
    struct TEntry * next;
    struct TEntry * prev;
}TENTRY;

typedef struct TQueue{
    TENTRY * start;
    TENTRY * end;
}TQUEUE;

typedef struct TCarPark{
    TQUEUE parked;
    TQUEUE free;
}TCARPARK;

void freeQueue(TQUEUE * queue){
    if (queue->start == NULL)
        return;
    TENTRY * tmp = queue->start;
    while (tmp->next != NULL){
        tmp = tmp->next;
        free(tmp->prev);
    }
    free(tmp);
}

TENTRY * deQueue(TQUEUE * queue){
    TENTRY * popped = queue->start;
    if (queue->start->next == NULL){
        queue->start = NULL;
        queue->end = NULL;
    } else {
        queue->start = queue->start->next;
        queue->start->prev = NULL;
    }
    return popped;
}


void enQueue(TQUEUE * queue, TENTRY * node){
    if (queue->start == NULL){
        queue->start = node;
        queue->end = node;
        node->next = NULL;
        node->prev = NULL;
    } else {
        queue->end->next = node;
        node->prev = queue->end;
        queue->end = node;
        queue->end->next = NULL;
    }
}

void removeFromQueue(TQUEUE * queue, TENTRY * entry){
    if (queue->start->next == NULL){
        queue->start = NULL;
        queue->end = NULL;
    } else if (queue->start == entry){
        queue->start = queue->start->next;
        queue->start->prev = NULL;
    } else if (queue->end == entry){
        queue->end = queue->end->prev;
        queue->end->next = NULL;
    } else {
        entry->prev->next = entry->next;
        entry->next->prev = entry->prev;
    }
}

TENTRY * findInQueue(TQUEUE * queue, const char rz[11]){
    TENTRY * tmp = queue->start;
    while (tmp){
        if (!strcmp(tmp->rz,rz))
            return tmp;
        tmp = tmp->next;
    }
    return NULL;
}

TENTRY * createEntry(char floor, int space){
    TENTRY * entry = (TENTRY*) malloc(sizeof (TENTRY));
    entry->floor = floor;
    entry->space = space;
    return entry;
}

void initCarPark(TCARPARK * carPark, char maxFloor, int maxSpace){
    carPark->parked.start = NULL;
    carPark->parked.end = NULL;
    carPark->free.start = NULL;
    carPark->free.end = NULL;
    for (char floor = 'A'; floor <= maxFloor; ++floor) {
        for (int space = 1; space <= maxSpace; ++space) {
            enQueue(&carPark->free, createEntry(floor,space));
        }
    }
}

void towingService(TCARPARK * carPark){
    TENTRY * towed = deQueue(&carPark->parked);
    printf("Odtah vozu %s z pozice %c%d\n",towed->rz,towed->floor, towed->space);
    enQueue(&carPark->free,towed);
}

void parkCar(TCARPARK * carPark, const char rz[11]){
    if (carPark->free.start == NULL)
        towingService(carPark);
    enQueue(&carPark->parked, deQueue(&carPark->free));
    strcpy(carPark->parked.end->rz,rz);
    printf("Pozice: %c%d\n", carPark->parked.end->floor, carPark->parked.end->space);
}

void removeCar(TCARPARK * carPark, const char rz[11]){
    TENTRY * car = findInQueue(&carPark->parked, rz);
    if (car == NULL){
        printf("Nenalezeno\n");
        return;
    }
    removeFromQueue(&carPark->parked,car);
    enQueue(&carPark->free,car);
    printf("Pozice: %c%d\n", carPark->free.end->floor, carPark->free.end->space);
}

int main() {
    char maxFloor;
    int maxSpace;
    printf("Velikost:\n");
    if (scanf("%c %d", &maxFloor, &maxSpace) != 2
        || maxFloor < 'A' || maxFloor > 'Z'
        || maxSpace <= 0
        || fgetc(stdin) != '\n'){
        printf("Nespravny vstup.\n");
        return 1;
    }
    TCARPARK carPark;
    initCarPark(&carPark,maxFloor,maxSpace);
    char command, rz[11];
    printf("Pozadavky:\n");
    while (1){
        int status = scanf("%c %10s\n",&command,rz);
        if (status == EOF) {
            freeQueue(&carPark.free);
            freeQueue(&carPark.parked);
            return 0;
        }
        if (status != 2 || (command != '+' && command != '-')) {
            printf("Nespravny vstup.\n");
            freeQueue(&carPark.free);
            freeQueue(&carPark.parked);
            return 1;
        }
        if (command == '+')
            parkCar(&carPark,rz);
        else
            removeCar(&carPark,rz);
    }
}
